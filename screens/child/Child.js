import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Button, Text, View} from 'react-native';
import {Screen} from '../../components';
import styles from './styles/Child.style';

export function Child() {
  const navigation = useNavigation();

  return (
    <Screen>
      <View style={styles.container}>
        <Text style={styles.text}>Child</Text>
        <Button title="Main" onPress={() => navigation.navigate('main')} />
      </View>
    </Screen>
  );
}
