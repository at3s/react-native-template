import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Button, Text, View} from 'react-native';
import {Screen} from '../../components';
import styles from './styles/Main.style';

export function Main() {
  const navigation = useNavigation();

  return (
    <Screen>
      <View style={styles.container}>
        <Text style={styles.text}>Main</Text>
        <Button title="Child" onPress={() => navigation.navigate('child')} />
      </View>
    </Screen>
  );
}
